<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\BookingController;
/*
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/restaurants', [RestaurantController::class, 'show']);
Route::post('/newRes', [RestaurantController::class, 'new']);
Route::post('/updateRes', [RestaurantController::class, 'update']);
Route::post('/deleteRes', [RestaurantController::class, 'delete']);

Route::get('/status', [StatusController::class, 'show']);

Route::get('/table', [TableController::class, 'show']);
Route::post('/newTable', [TableController::class, 'new']);
Route::post('/updateTable', [TableController::class, 'update']);
Route::post('/deleteTable', [TableController::class, 'delete']);

Route::get('/client', [ClientController::class, 'show']);
Route::post('/newClient', [ClientController::class, 'new']);
Route::post('/updateClient', [ClientController::class, 'update']);
Route::post('/deleteClient', [ClientController::class, 'delete']);

Route::get('/booking', [BookingController::class, 'show']);
Route::post('/newBooking', [BookingController::class, 'new']);
Route::post('/updateBooking', [BookingController::class, 'update']);
Route::post('/deleteBooking', [BookingController::class, 'delete']);

