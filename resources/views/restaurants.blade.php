<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/adfb5ee13e.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <title>Restaurants</title>
    <style>
        form{
            width:90%;
            margin-left:5%;
            margin-right:5%;
             padding: 5%;
        } 
        .container{
            text-align: center;
            color: white
        }
    </style>
</head>
<body background="https://i.pinimg.com/564x/ad/11/be/ad11becc802b48a132c92226d4f012b2.jpg">
    
    <div id="app">
        <div class="container p-5">
            <h1><strong>Bienvenido</strong></h2>
                <h2 class="p-2">La lista de restaurantes registrados es:</h2>
                <table class="table" style="color: white;">
                    <thead>
                        <th scope="col">Nombre</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Opciones</th>
                    </thead>
                    <tbody>
                        <tr v-for="(restaurant, index) in restaurants">
                            <td>@{{restaurant.name}}</td>
                            <td>@{{restaurant.address}}</td>
                            <td>@{{restaurant.phone}}</td>
                            <td>
                                <button type="button" :style="{backgroundColor: restaurant.status.color}" class="btn mb-2" @click="edit(restaurant); deleteRes(restaurant.id);" >@{{restaurant.status.name}}</button>
                                <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#editModal" @click="edit(restaurant)">Actualizar</button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalLabel">Ingrese datos nuevos</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" >
                                <form @submit.prevent="updateRes">
                                    <input type="text" class="form-control" style="margin-bottom: 2%" value="" name="name" required v-model.trim="restaurant.name"/>
                                    <input type="number" class="form-control" style="margin-bottom: 2%" value="" name="phone" required v-model.trim="restaurant.phone"/>
                                    <input type="text" class="form-control" style="margin-bottom: 2%" value="" name="address" required v-model.trim="restaurant.address"/>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                        <button type="submit" class="btn btn-dark" value="submit">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="p-5">
            <button type="button" class="btn btn-light m-5" data-toggle="modal" data-target="#exampleModal">
            Registrar nuevo restaurante
            </button>
    
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ingreso de datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form  @submit.prevent="createNew">
                                <input type="text" class="form-control"  style="margin-bottom: 2%" placeholder="Nombre " name="name" required v-model.trim="restaurant.name">
                                <input type="number" class="form-control" style="margin-bottom: 2%" placeholder="Telefono" value="" name="phone" required v-model.trim="restaurant.phone"/>
                                <input type="text" class="form-control" style="margin-bottom: 2%" placeholder="Direccion" value="" name="adress" required v-model.trim="restaurant.address"/>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                    <button type="submit" class="btn btn-dark">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-light"><a href="/table" style="text-decoration: none; color: black;">Ver mesas</a></button>
            <button class="btn btn-light"><a href="/client" style="text-decoration: none; color: black;">Ver clientes</a></button>
            <button class="btn btn-light"><a href="/booking" style="text-decoration: none; color: black;">Ver reservas</a></button>
        </div>
    </div>

</body>
<script>
     var app = new Vue({
        el: '#app',
        data: {
            restaurants: [],
            statuses: [],
            restaurant: {
                id: null, 
                name: null,
                address: null,
                phone: null,
                status_id: null
            }
        },

        mounted(){
            this.getRestaurants()
            this.getStatus()
        },

        methods: {
            getRestaurants(){
                let data = fetch("/api/restaurants").then(async(res)=>{
                    let result = await res.json()
                    this.restaurants = result
                })
            },
            
            getStatus(){
                let data= fetch("/api/status").then(async (res)=>{
                    let result = await res.json()
                    this.statuses = result
                })
            },

            clear(){
                this.restaurant.id = null
                this.restaurant.name = null
                this.restaurant.address = null
                this.restaurant.phone = null
                this.restaurant.status_id = null
            },
            
            edit(restaurant){
                this.restaurant.id = restaurant.id
                this.restaurant.name = restaurant.name
                this.restaurant.address = restaurant.address
                this.restaurant.phone = restaurant.phone
                this.restaurant.status_id = restaurant.status.id
            },
            
            deleteRes(restaurant){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/deleteRes", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.restaurant),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getRestaurants()
                })
            },

            updateRes(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/updateRes", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.restaurant),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getRestaurants()
                    $('#editModal').modal('hide')  
                })
            },

            createNew(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/newRes", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.restaurant),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getRestaurants()
                    $('#exampleModal').modal('hide')
                })
            }
        }

    })
</script>
</html>