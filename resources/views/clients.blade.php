<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/adfb5ee13e.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <title>Restaurants</title>
    <style>
        form{
            width:90%;
            margin-left:5%;
            margin-right:5%;
             padding: 5%;
        } 
        .container{
            text-align: center;
            color: white
        }
    </style>
</head>
<body background="https://i.pinimg.com/564x/ad/11/be/ad11becc802b48a132c92226d4f012b2.jpg">
    
    <div id="app">
        <div class="container p-5">
            <h1><strong>Seccion de clientes</strong></h2>
                <h2 class="p-2">La lista de clientes registrados es:</h2>
                <table class="table" style="color: white;">
                    <thead>
                        <th scope="col">Nombre</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Opciones</th>
                    </thead>
                    <tbody>
                        <tr v-for="(client, index) in clients">
                            <td>@{{client.name}}</td>
                            <td>@{{client.address}}</td>
                            <td>@{{client.phone}}</td>
                            <td>
                                <button type="button" :style="{backgroundColor: client.status.color}" class="btn mb-2" @click="edit(client); deleteClient(client);" >@{{client.status.name}}</button>
                                <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#editModal" @click="edit(client)">Actualizar</button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalLabel">Ingrese datos nuevos</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" >
                                <form @submit.prevent="updateClient">
                                    <input type="text" class="form-control" style="margin-bottom: 2%" value="" name="name" required v-model.trim="client.name"/>
                                    <input type="number" class="form-control" style="margin-bottom: 2%" value="" name="phone" required v-model.trim="client.phone"/>
                                    <input type="text" class="form-control" style="margin-bottom: 2%" value="" name="address" required v-model.trim="client.address"/>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                        <button type="submit" class="btn btn-dark" value="submit">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="p-5">
            <button type="button" class="btn btn-light m-5" data-toggle="modal" data-target="#exampleModal">
            Registrate
            </button>
    
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ingreso de datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form  @submit.prevent="createClient">
                                <input type="text" class="form-control"  style="margin-bottom: 2%" placeholder="Nombre " name="name" required v-model.trim="client.name">
                                <input type="number" class="form-control" style="margin-bottom: 2%" placeholder="Telefono" value="" name="phone" required v-model.trim="client.phone"/>
                                <input type="text" class="form-control" style="margin-bottom: 2%" placeholder="Direccion" value="" name="adress" required v-model.trim="client.address"/>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                    <button type="submit" class="btn btn-dark">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn btn-light"><a href="/table" style="text-decoration: none; color: black;">Ver mesas</a></button>
            <button class="btn btn-light"><a href="/" style="text-decoration: none; color: black;">Ver restaurantes</a></button>
            <button class="btn btn-light"><a href="/booking" style="text-decoration: none; color: black;">Ver reservas</a></button>
        </div>
    </div>

</body>
<script>
     var app = new Vue({
        el: '#app',
        data: {
            clients: [],
            statuses: [],
            client: {
                id: null, 
                name: null,
                address: null,
                phone: null,
                status_id: null
            }
        },

        mounted(){
            this.getClients()
            this.getStatus()
        },

        methods: {
            getClients(){
                let data = fetch("/api/client").then(async(res)=>{
                    let result = await res.json()
                    this.clients = result
                })
            },
            
            getStatus(){
                let data= fetch("/api/status").then(async (res)=>{
                    let result = await res.json()
                    this.statuses = result
                })
            },

            clear(){
                this.client.id = null
                this.client.name = null
                this.client.address = null
                this.client.phone = null
                this.client.status_id = null
            },
            
            edit(client){
                this.client.id = client.id
                this.client.name = client.name
                this.client.address = client.address
                this.client.phone = client.phone
                this.client.status_id = client.status.id
            },
            
            deleteClient(client){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/deleteClient", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.client),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getClients()
                })
            },

            updateClient(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/updateClient", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.client),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getClients()
                    $('#editModal').modal('hide')  
                })
            },

            createClient(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/newClient", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.client),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getClients()
                    $('#exampleModal').modal('hide')
                })
            }
        }

    })
</script>
</html>