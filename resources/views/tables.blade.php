<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/adfb5ee13e.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <title>Tables</title>
    <style>
        form{
            width:90%;
            margin-left:5%;
            margin-right:5%;
             padding: 5%;
        } 
        .container{
            text-align: center;
            color: white
        }
    </style>
</head>
<body background="https://i.pinimg.com/564x/ad/11/be/ad11becc802b48a132c92226d4f012b2.jpg">
    
    <div id="app">
        <div class="container p-5">
            <h1><strong>Seccion de mesas</strong></h2>
            <h2 class="p-2">La lista de mesas disponibles es:</h2>
            <table class="table" style="color: white;">
                <thead>
                    <th scope="col">Nombre</th>
                    <th scope="col">Restaurante</th>
                    <th scope="col">Opciones</th>
                </thead>
                <tbody>
                    <tr v-for="(table, index) in tables">
                        <td>@{{table.name}}</td>
                        <td>@{{table.restaurant.name}}</td>
                        <td>
                            <button type="button" :style="{backgroundColor: table.status.color}" class="btn mb-2" @click="edit(table); deleteTable(table);" >@{{table.status.name}}</button>
                            <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#editModal" @click="edit(table)">Actualizar</button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editModalLabel">Ingrese datos nuevos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" >
                            <form @submit.prevent="updateTable">
                                <input type="text" class="form-control" style="margin-bottom: 2%" value="" name="name" required v-model.trim="table.name"/>
                                <select name="restaurant_id" class="form-control" style="margin-bottom: 2%" required v-model.trim="table.restaurant_id">
                                    <option v-for="(restaurant, index) in restaurants" v-if="(restaurant.status_id == 1)" :value="restaurant.id" >@{{restaurant.name}}</option>
                                </select>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                    <button type="submit" class="btn btn-dark" value="submit">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="p-5">
            <button type="button" class="btn btn-light m-5" data-toggle="modal" data-target="#exampleModal">
            Registrar nueva mesa
            </button>
    
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ingreso de datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form  @submit.prevent="createTable">
                                <input type="text" class="form-control"  style="margin-bottom: 2%" placeholder="Nombre " name="name" required v-model.trim="table.name">
                                <select name="restaurant_id" class="form-control" style="margin-bottom: 2%" required v-model.trim="table.restaurant_id">
                                    <option v-for="(restaurant, index) in restaurants" v-if="(restaurant.status_id == 1)" :value="restaurant.id" >@{{restaurant.name}}</option>
                                </select>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                    <button type="submit" class="btn btn-dark">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-light"><a href="/" style="text-decoration: none; color: black;">Ver restaurantes</a></button>
            <button class="btn btn-light"><a href="/client" style="text-decoration: none; color: black;">Ver clientes</a></button>
            <button class="btn btn-light"><a href="/booking" style="text-decoration: none; color: black;">Ver reservas</a></button>
        </div>
    </div>

</body>
<script>
     var app = new Vue({
        el: '#app',
        data: {
            tables: [],
            restaurants: [],
            table: {
                id: null, 
                name: null,
                restaurant_id: null
            }
        },

        mounted(){
            this.getRestaurants()
            this.getTables()
        },

        methods: {
            getTables(){
                let data= fetch("/api/table").then(async (res)=>{
                    let result = await res.json()
                    this.tables = result
                })
            },

            getRestaurants(){
                let data = fetch("/api/restaurants").then(async(res)=>{
                    let result = await res.json()
                    this.restaurants = result
                    console.log(this.restaurants)
                })
            },

            clear(){
                this.table.id = null
                this.table.name = null
                this.table.restaurant_id = null
            },
            
            edit(table){
                this.table.id = table.id
                this.table.name = table.name
                this.table.restaurant_id = table.restaurant_id
                
            },
            
            deleteTable(table){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/deleteTable", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.table),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getTables()
                })
            },

            updateTable(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/updateTable", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.table),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getTables()
                    $('#editModal').modal('hide')  
                })
            },

            createTable(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/newTable", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.table),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getTables()
                    $('#exampleModal').modal('hide')
                })
            }
        }
    })
</script>
</html>