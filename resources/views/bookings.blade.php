<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/adfb5ee13e.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <title>Bookings</title>
    <style>
        form{
            width:90%;
            margin-left:5%;
            margin-right:5%;
             padding: 5%;
        } 
        .container{
            text-align: center;
            color: white
        }
    </style>
</head>
<body background="https://i.pinimg.com/564x/ad/11/be/ad11becc802b48a132c92226d4f012b2.jpg">
    
    <div id="app">
        <div class="container p-5">
            <h1><strong>Seccion de reservas</strong></h2>
                <h2 class="p-2">El calendario de reservas es:</h2>
                <table class="table" style="color: white;">
                    <thead>
                        <th scope="col">A nombre de</th>
                        <th scope="col">Restaurante</th>
                        <th scope="col">Mesa</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Opciones</th>
                    </thead>
                    <tbody>
                        <tr v-for="(booking, index) in bookings">
                            <td>@{{booking.client.name}}</td>
                            <td v-for="(table, index) in tables" v-if="booking.table_id == table.id">@{{table.restaurant.name}}</td>
                            <td>@{{booking.table.name}}</td>
                            <td>@{{booking.date}}</td>
                            <td>
                                <button type="button" :style="{backgroundColor: booking.status.color}" class="btn mb-2" @click="edit(booking); deleteBooking(booking);" >@{{booking.status.name}}</button>
                                <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#editModal" @click="edit(booking)">Actualizar</button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalLabel">Ingrese datos nuevos</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" >
                                <form @submit.prevent="updateBooking">
                                    <select name="client_id" class="form-control" style="margin-bottom: 2%" required v-model.trim="booking.client_id">
                                        <option v-for="(client, index) in clients" v-if="client.status_id == 1" :value="client.id">@{{client.name}}</option>
                                    </select>
                                    <select name="aux" class="form-control" style="margin-bottom: 2%" required v-model.trim="aux">
                                        <option v-for="(restaurant, index) in restaurants" v-if="restaurant.status_id == 1" :value="restaurant.id">@{{restaurant.name}}</option>
                                        <!--<option v-for="(table, index) in tables" v-if="table.status_id == 1" :value="table.restaurant_id" :selected="booking.table_id == table.id">@{{table.restaurant.name}}</option>-->
                                    </select>
                                    <select name="table_id" class="form-control" style="margin-bottom: 2%" required v-model.trim="booking.table_id">
                                        <option v-for="(table, index) in tables" v-if="table.status_id == 1 && table.restaurant_id == aux" :value="table.id">@{{table.name}}</option>
                                    </select>
                                    <input type="date" class="form-control" style="margin-bottom: 2%" value="" name="date" required v-model.trim="booking.date"/>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                        <button type="submit" class="btn btn-dark" value="submit">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="p-5">
            <button type="button" class="btn btn-light m-5" data-toggle="modal" data-target="#exampleModal">
            Reservar
            </button>
    
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ingreso de datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="clear">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form  @submit.prevent="createBooking">
                                <label>A nombre de:</label>
                                <select name="client_id" class="form-control" style="margin-bottom: 2%" required v-model.trim="booking.client_id">
                                    <option v-for="(client, index) in clients" v-if="client.status_id == 1" :value="client.id" >@{{client.name}}</option>
                                </select>
                                <label>Restaurante:</label>
                                <select name="aux" class="form-control" style="margin-bottom: 2%" required v-model.trim="aux">
                                    <option v-for="(restaurant, index) in restaurants" v-if="restaurant.status_id == 1" :value="restaurant.id" >@{{restaurant.name}}</option>
                                </select>
                                <label>Mesa:</label>
                                <select name="table_id" class="form-control" style="margin-bottom: 2%" required v-model.trim="booking.table_id">
                                    <option v-for="(table, index) in tables" v-if="table.status_id == 1 && table.restaurant_id == aux" :value="table.id" >@{{table.name}}</option>
                                </select>
                                <label>Fecha: </label>
                                <input type="date" class="form-control" style="margin-bottom: 2%" value="" name="date" required v-model.trim="booking.date"/>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="clear">Cerrar</button>
                                    <button type="submit" class="btn btn-dark">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn btn-light"><a href="/table" style="text-decoration: none; color: black;">Ver mesas</a></button>
            <button class="btn btn-light"><a href="/" style="text-decoration: none; color: black;">Ver restaurantes</a></button>
            <button class="btn btn-light"><a href="/booking" style="text-decoration: none; color: black;">Ver reservas</a></button>
        </div>
    </div>

</body>
<script>
     var app = new Vue({
        el: '#app',
        data: {
            bookings: [],
            tables: [],
            restaurants: [],
            clients: [],
            statuses: [],
            booking: {
                id: null, 
                client_id: null,
                table_id: null,
                date: null
            },
            aux: null
        },

        mounted(){
            this.getClients()
            this.getStatus()
            this.getTables()
            this.getBookings()
            this.getRestaurants()
        },

        methods: {
            getBookings(){
                let data = fetch("/api/booking").then(async(res)=>{
                    let result = await res.json()
                    this.bookings = result
                    console.log(this.bookings)
                })
            },

            getTables(){
                let data= fetch("/api/table").then(async (res)=>{
                    let result = await res.json()
                    this.tables = result
                    console.log(this.tables)
                })
            },

            getClients(){
                let data = fetch("/api/client").then(async(res)=>{
                    let result = await res.json()
                    this.clients = result
                })
            },

            getRestaurants(){
                let data = fetch("/api/restaurants").then(async(res)=>{
                    let result = await res.json()
                    this.restaurants = result
                })
            },
            
            getStatus(){
                let data= fetch("/api/status").then(async (res)=>{
                    let result = await res.json()
                    this.statuses = result
                })
            },

            clear(){
                this.booking.id = null
                this.booking.client_id = null
                this.booking.table_id = null
                this.booking.date = null
                this.booking.status_id = null
                this.aux = null
            },
            
            edit(booking){
                this.booking.id = booking.id
                this.booking.client_id = booking.client_id
                this.booking.table_id = booking.table_id
                this.booking.date = booking.date
                this.booking.status_id = booking.status.id
            },
            
            deleteBooking(booking){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/deleteBooking", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.booking),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getBookings()
                })
            },

            updateBooking(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/updateBooking", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.booking),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getBookings()
                    $('#editModal').modal('hide')  
                })
            },

            createBooking(){
                let headers = {"Content-Type":'application/json'}
                let data= fetch("/api/newBooking", {
                    headers,
                    method: 'POST',
                    body: JSON.stringify(this.booking),
                }).then(async (res)=>{
                    let result = await res.json()
                    this.clear(),
                    this.getBookings()
                    $('#exampleModal').modal('hide')
                })
            }
        }

    })
</script>
</html>