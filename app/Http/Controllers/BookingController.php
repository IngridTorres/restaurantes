<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Booking;
use Validator;

class Bookingcontroller extends Controller
{
    public function show()
    {
        $booking = Booking::with('status', 'client', 'table')->get();
        return $booking;
    }

    public function new(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'client_id' => 'required',
                'table_id' => 'required',
                'date' => 'required'
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $booking = new Booking;
            $booking->status_id = 1;
            $booking->client_id = $request['client_id'];
            $booking->table_id = $request['table_id'];
            $booking->date = $request['date'];
            $booking->save();

            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }
    }

    
    public function delete(Request $request)
    {
        $booking = Booking::findOrFail($request->id);
        $booking->status_id = $booking->status_id == 1 ? 2 : 1;
        $booking->save();

        return true;
    }

    public function update(Request $request)
    {   
        try {
            $validator = Validator::make($request->all(), [
                'client_id' => 'required',
                'table_id' => 'required',
                'date' => 'required'
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $booking = Booking::findOrFail($request->id);
            $booking->client_id = $request['client_id'];
            $booking->table_id = $request['table_id'];
            $booking->date = $request['date'];
            $booking->save();

            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }
    }
}