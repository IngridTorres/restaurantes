<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Restaurant;
use Validator;

class RestaurantController extends Controller
{
    public function show()
    {
        $res = Restaurant::with('status')->get();
        return $res;
    }

    public function new(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'phone' => 'required|unique:restaurants',
                'address' => 'required|unique:restaurants',
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $res = new Restaurant;
            $res->name = $request['name'];
            $res->status_id = 1;
            $res->phone = $request['phone'];
            $res->address = $request['address'];
            $res->save();

            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }
    }

    
    public function delete(Request $request)
    {
        $res = Restaurant::findOrFail($request->id);
        $res->status_id = $res->status_id == 1 ? 2 : 1;
        $res->save();
        return true;
    }

    public function update(Request $request)
    {   
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'phone' => 'required',
                'address' => 'required',
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $res = Restaurant::findOrFail($request->id);
            $res->name = $request['name'];
            $res->phone = $request['phone'];
            $res->address = $request['address'];
            $res->save();
            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }
    }
}