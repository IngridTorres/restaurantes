<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use App\Models\Table;
use Symfony\Component\HttpFoundation\Request;
use Validator;

class TableController extends BaseController
{
    public function show()
    {
        $table = Table::with('restaurant', 'status')->get();
        return $table;
    }

    public function delete(Request $request)
    {
        $table = Table::findOrFail($request->id);
        $table->status_id = $table->status_id == 1 ? 2 : 1;
        $table->save();
        return true;
    }

    public function new(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'restaurant_id' => 'required'
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $table = new Table;
            $table->name = $request['name'];
            $table->status_id = 1;
            $table->restaurant_id = $request['restaurant_id'];
            $table->save();

            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }
    }

    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'restaurant_id' => 'required',
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $table = Table::findOrFail($request->id);
            $table->name = $request['name'];
            $table->restaurant_id = $request['restaurant_id'];
            $table->save();
            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }    }

}