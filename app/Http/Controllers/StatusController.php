<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use App\Models\Status;

class StatusController extends BaseController
{
    public function show()
    {
        return Status::all();
    }
}