<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Client;
use Validator;

class Clientcontroller extends Controller
{
    public function show()
    {
        $client = Client::with('status')->get();
        return $client;
    }

    public function new(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'phone' => 'required|unique:clients',
                'address' => 'required|unique:clients',
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $client = new Client;
            $client->name = $request['name'];
            $client->status_id = 1;
            $client->phone = $request['phone'];
            $client->address = $request['address'];
            $client->save();

            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }
    }

    
    public function delete(Request $request)
    {
        $client = Client::findOrFail($request->id);
        $client->status_id = $client->status_id == 1 ? 2 : 1;
        $client->save();

        return true;
    }

    public function update(Request $request)
    {   
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'phone' => 'required',
                'address' => 'required',
            ]); 

            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }

            $client = Client::findOrFail($request->id);
            $client->name = $request['name'];
            $client->phone = $request['phone'];
            $client->address = $request['address'];
            $client->save();

            return true;
        } 
        catch (Exception $e) {
            return Response::json(['created' => false], 500);
        }
    }
}