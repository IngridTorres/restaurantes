<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'client_id',
        'table_id',
        'date',
        'status_id'
    ];

    public function status(){
        return $this->belongsTo(Status::class);
    }
    
    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function table(){
        return $this->belongsTo(Table::class);
    }
}